$(document).ready(function () {
$('.our-work__slider').slick({
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2       
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1       
      }
    }    
  ]
})
});

const config = {
    apiKey: process.env.API_KEY,
    authDomain: process.env.AUTH_DOMAIN,
    databaseURL: process.env.DATABASE_URL,
    projectId: process.env.PROJECT_ID,
    storageBucket: process.env.STORAGE_BUCKET,
    messagingSenderId: process.env.MESSAGING_SENDER_ID
}

firebase.initializeApp(config);
const messageRef = firebase.database().ref("messages");
document.getElementById("contactForm").addEventListener("submit", formSubmit);


function formSubmit (e) {
    e.preventDefault();   
    
    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const message = document.getElementById("message").value;
    
    saveMessage(name, email, message);
    document.querySelector(".contact__items__form h3").textContent = "Thanks for your message";
    document.querySelector(".contact__items__form h3").style.color = "green";
    document.querySelector(".contact__items__form").reset();
}


function saveMessage (name, email, message) {
    const messagePayload = {name, email, message};
    messageRef.push(messagePayload);
}


